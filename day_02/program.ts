import * as fs from 'fs';

// PART 1
const boxIds = Array.from(fs.readFileSync('day_02\\input.txt', { encoding: 'utf-8'})
    .split(/\n|\r/g)
    .filter(boxId => boxId)
    .map(boxId => boxId.trim()));

let doubleLetters = 0;
let tripleLetters = 0;

for(var boxId of boxIds) {
    const orderedBoxId = [...boxId].sort().join();
    const doubleMatch = (orderedBoxId.match(/(\w)\1{1}/g) || []).map(c => c.substring(0,1));
    const tripleMatch = (orderedBoxId.match(/(\w)\1{2}/g) || []).map(c => c.substring(0,1));

    if (tripleMatch.length > 0) {
        tripleLetters++;
    }

    if (doubleMatch.filter(doubleChar => !tripleMatch.includes(doubleChar)).length > 0) {
        doubleLetters++;
    }
}

console.log('PART 1:', doubleLetters * tripleLetters);

// PART 2
const getDifferingCharacters = (left: string, right: string): string[] => {
    let differingCharacters = [];

    [...left].map((c, i) => {
        if (c !== right[i]) {
            differingCharacters.push(c);
        }
    })

    return differingCharacters;
}

outerloop:
for(let boxId of boxIds) {
    const others = boxIds.filter(id => id !== boxId);
    for(let other of others) {
        const differingCharacters = getDifferingCharacters(boxId, other);
        if (differingCharacters.length === 1) {
            console.log('PART 2:', boxId.replace(differingCharacters[0], ''));
            break outerloop;
        }
    }
}
