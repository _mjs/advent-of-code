import * as fs from 'fs';
import { POINT_CONVERSION_COMPRESSED } from 'constants';

const log = fs.readFileSync('day_04\\input.txt', { encoding: 'utf-8' })
    .split(/\n|\r/g)
    .filter(entry => entry)
    .map(entry => {
        const match = entry.match(/([^\[.*?\]]+)/g);
        const guardMatch = match[1].match(/(\#\d+)\s/g);
        const guardId = guardMatch ? guardMatch[0].replace('#', '').trim() : undefined
        return {
            timestamp: new Date(match[0]),
            guardId: guardId,
            operation: match[1].replace(guardId, '').trim()
        };
    })
    .sort((log1, log2) =>
        (log1.timestamp > log2.timestamp) ? 1
            : ((log2.timestamp > log1.timestamp) ? -1 : 0));

let currentGuard = '';
let fellAlseepOn = 0;
let sleepRegister = [];

for (let observation of log) {
    const minute = new Date(observation['timestamp']).getMinutes();

    if (observation['guardId']) {
        currentGuard = observation['guardId'];
        if (!sleepRegister[currentGuard]) {
            sleepRegister[currentGuard] = Array(59).fill(0);
        }
    }

    if (observation['operation'] === 'falls asleep') {
        fellAlseepOn = minute;
    }

    if (observation['operation'] === 'wakes up') {
        for (var i = fellAlseepOn; i < minute; i++) {
            sleepRegister[currentGuard][i]++;
        }
    }
}

const guardReport = Object.keys(sleepRegister).map(key => {
    const guard: [number] = sleepRegister[key];

    return {
        guardId: key,
        totalMinsAlseep: guard.reduce((a, b) => a + b, 0),
        mostSleptMinute: guard.indexOf(Math.max(...guard)),
        mostSleptMinuteCount: Math.max(...guard)
    }
});

const guardAsleepMost = guardReport.reduce((prev, curr) => (prev.totalMinsAlseep > curr.totalMinsAlseep) ? prev : curr)
console.log('PART 1:', guardAsleepMost.mostSleptMinute * +guardAsleepMost.guardId);

const guardAsleepOnMostMinute = guardReport.reduce((prev, curr) => (prev.mostSleptMinuteCount > curr.mostSleptMinuteCount) ? prev : curr);
console.log('PART 2:', guardAsleepOnMostMinute.mostSleptMinute * +guardAsleepOnMostMinute.guardId);

