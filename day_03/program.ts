import * as fs from 'fs';

interface Claim {
    claimId: string,
    position: string,
    dimentions: string,
    isOverlapping: boolean
}

const claims = fs.readFileSync('day_03\\input.txt', { encoding: 'utf-8' })
    .split(/\n|\r/g)
    .filter(claim => claim)
    .map(claim => {
        const match = claim.match(/(#\d+)|((?:(\d+),\d+))|((?:(\d+)x(\d+)))/g);
        return {
            claimId: match[0],
            position: match[1],
            dimentions: match[2],
            isOverlapping: false
        } as Claim;
    });

const fabric = Array.from({ length: 1000 }, () => Array.from({ length: 1000 }, () => ({ claimedBy: new Set() })));

const makeClaim = (claim: Claim): number => {
    const x = +claim.position.split(',')[0];
    const y = +claim.position.split(',')[1];
    const width = +claim.dimentions.split('x')[0];
    const height = +claim.dimentions.split('x')[1];
    let overlap = 0;

    for (let col = 0; col < width; col++) {
        for (let row = 0; row < height; row++) {
            const squareInchClaimedBy: Set<{}> = fabric[x + col][y + row].claimedBy
            squareInchClaimedBy.add(claim.claimId);

            if (squareInchClaimedBy.size >= 2) {
                overlap += squareInchClaimedBy.size === 2 ? 1 : 0
                claims
                    .filter(c => squareInchClaimedBy.has(c.claimId))
                    .forEach(c => c.isOverlapping = true);
            }
        }
    }

    return overlap;
}

let overlappingClaimSquareInches = 0;
for (let claim of claims) {
    overlappingClaimSquareInches += makeClaim(claim);
}

console.log('PART 1:', overlappingClaimSquareInches);
console.log('PART 2:', claims.filter(c => !c.isOverlapping)[0].claimId);