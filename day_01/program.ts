import * as fs from 'fs';

const input = fs.readFileSync('day_01\\input.txt', { encoding: 'utf-8'});
const startFrequency = 0;

// PART 1
const frequencyChanges = input.replace(/\n|\r/g, '');
console.log('PART 1:', eval(startFrequency + frequencyChanges));

// PART 2
let runningChanges = {'0': 0};
let recurringFrequency = undefined;
let currentFrequency = startFrequency;
const changes = Array.from(input.split(/\n|\r/g)).filter(freq => freq.trim() !== '');

do {
    for(var i of changes) {
        const newFrequency = eval(currentFrequency + i);
        if (newFrequency in runningChanges) {
            recurringFrequency = newFrequency; break;
        } else {
            runningChanges[newFrequency] = currentFrequency = newFrequency;
        }
    }
} while (!recurringFrequency)
console.log('PART 2:', recurringFrequency);
